# Válvula Charlotte para máscaras de snorkel decathlon/cressi
Esta válvula permite conectar aparatage hospitalario a las máscaras de snorkel de decathlon/cressi para usarlas como máscaras para insuflar oxígeno y así subir la saturación del mismo en pacientes afectados por COVID-19.
## Materiales de impresión
Todas las impresiones se deben de hacer con filamento de PLA (si puede ser sin pigmento mejor), es lo que se ha probado y validado por varios hospitales.
Las resinas no sirven por su toxicidad.

## Tipos de máscaras
### Decathlon easybreath (AKA v1)
<img src="decathlon_easybreath/decathlon_easybreath_AKA_v1.png" height="120">  <img src="decathlon_easybreath/decathlon_easybreath_AKA_v1_conector.png" height="120"> <img src="decathlon_easybreath/decathlon_easybreath_AKA_v1_tubo.png" height="120">

Se caracteriza por tener el conector del snorkel con forma de cilindro extendido y por tener una forma recta y el enganche es securizado por una pestaña.

#### Modelo

<img src="decathlon_easybreath/charlotte_evolved_3d.png" height="120">

#### Fichero para imprimir
VERSION ACTUAL VALIDADA: **v3.2.0**

FICHERO DE IMPRESION: **[charlotte_evolved_v3.2.0.stl](decathlon_easybreath)**

### Decathlon easybreath 500 (AKA v2)
<img src="decathlon_easybreath_500/decathlon_easybreath_500_AKA_v2.png" height="120">  <img src="decathlon_easybreath_500/decathlon_easybreath_500_AKA_v2_conector.png" height="120"> <img src="decathlon_easybreath_500/decathlon_easybreath_500_AKA_v2_tubo.png" height="120">

#### Modelo

N/A

#### Fichero para imprimir
VERSION ACTUAL VALIDADA: **TBD**
FICHERO DE IMPRESION: **TBD**

## Uso y configuraciones de Máscara
Existen varias configuraciones de uso para la máscara dependiendo del hospital y tratamiento que se desea realizar. Estas configuraciones son las recomendadas.
La máscara tiene una conexión 22M para la entrada de Oxígeno y una salida 22F para la salida a un filtro y un PEEP.
La entrada de O2 22M debe llevar una válvula anti-retorno para impedir que el aire exhalado suba por el conducto de O2. 
En [la carpeta library-válvulas](https://gitlab.com/ma-d-kers/library) se encuentra esta válvula en el caso de solo utilizar un tubo O2 de entrada y no usar bolsa de reservorio. 

**Config 1** con una sola entrada de O2 y sin bolsa de reservorio(falta la PEEP en la foto): 

<img src="https://i.imgur.com/2tMoZJM.jpg" height="200">

**Config 2** con una sola entra de O2 y bolsa de reservorio. En [la carpeta dave](https://gitlab.com/ma-d-kers/dave) se puede encontrar la pieza T con la valvula integrada si no se dispone en el hospital de la T. (falta la PEEP y la bolsa en la foto):

<img src="https://i.imgur.com/en8dRa0.jpg" height="200">
<img src="https://i.imgur.com/XGfFnxv.jpg" height="200">

**Config 3** con doble entrada de O2 (Falta PEEP y la otra válvula antiretorno con entrada de O2 en la foto):

<img src="https://i.imgur.com/Vig1DLp.jpg" height="200">

## Válvulas internas

Se debe tapar la válvula antiretorno en la barbilla exterior de la mascara para que al exhalar no salga aire no filtrada. 

Se deben quitar las dos válvulas internas de la máscara, estas membranas luego se usarán para crear las válvulas anti-retorno en la entrada de O2.

<img src="https://i.imgur.com/QeY0nUx.jpg" height="200">
