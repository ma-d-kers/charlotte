# Decathlon easybreath 500 (AKA version 2)

![alt text](https://gitlab.com/ma-d-kers/charlotte/-/raw/master/decathlon_easybreath/decathlon_easybreath_500_AKA_v2.png "decathlon_easybreath")
![alt text](https://gitlab.com/ma-d-kers/charlotte/-/raw/master/decathlon_easybreath/decathlon_easybreath_500_AKA_v2_conector.png "decathlon_easybreath")

## Uso

Los ficheros de slide STL contenidos en esta carpeta están diseñados para imprimir el acople charlotte evolved en impresoras FDM.

## Parámetros de impresión

Estos son los parámetros más estandar de impresión que se están usando, por supuesto dependiendo de la impresora y el filamento pueden variar.

* Filamento: PLA 1.75mm
* Altura de capa: 0.28
* Grosor de pared: 1.2
* Relleno: 50-60%
* Temperatura boquilla: 205-210 ºc
* Temperatura cama: 60 primera capa, 50 siguientes (depende del filamento y fabricante)
* Soportes: No hacen falta

https://www.decathlon.es/es/p/mascara-snorkel-ensuperficie-easybreath-500-oyster/_/R-p-148873?mc=8491268&c=ROSA
