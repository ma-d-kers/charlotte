# Decathlon easybreath (AKA version 1)

## **VERSION ACTUAL VALIDADA: v3.2.0**
## **FICHERO DE IMPRESION: [charlotte_evolved_v3.2.0.stl](https://gitlab.com/ma-d-kers/charlotte/-/raw/master/decathlon_easybreath/charlotte_evolved_v3.2.0.stl?inline=false)**

<img src="decathlon_easybreath_AKA_v1.png" height="120"> <img src="decathlon_easybreath_AKA_v1_conector.png" height="120">

<img src="decathlon_easybreath_AKA_v1_charlotte.jpg" height="240">

## Uso

Los ficheros de slide STL contenidos en esta carpeta están diseñados para imprimir el acople charlotte evolved en impresoras FDM.

## Parámetros de impresión

Como norma general y teniendo en cuenta donde se van a usar esas piezas se requiere imprimir con una calidad aceptable, siendo preferible generar menos piezas de buena calidad a muchas piezas de peor calidad que pueden presentar problemas diversos.

Estos son los parámetros de impresión que se están usando para validar las piezas cuando se termina el diseño:

* Filamento: PLA 1.75mm
* Altura de capa: 0.2
* Grosor de pared: 1.6
* Relleno: 80%
* Soportes: No hacen falta

Aunque usar los mismos parámetros debería asegurar repetibilidad y calidad esto puede variar dependiendo de la impresora y el filamento que use cada maker por consiguiente se deberán ajustar para conseguir los resultados óptimos.

Las distintas pruebas han demostrado que usar una balsa de adherencia evita problemas de piezas despegadas de las camas.

## Calibración

### Configurar la expansión horizontal

Es fundamental realizar este proceso antes de comenzar a imprimir piezas porque las distintas configuraciones de impresoras combinadas con el uso de distintas marcas de filamento puede generar desviaciones en la impresión que afecten al ajuste final de la pieza con la máscara. 

Imprimir [Calibre_Charlotte_Reforzada.stl](Calibre_Charlotte_Reforzada.stl)


Es muy importante que utilices estas piezas de calibrado y no otras obtenidas en otro canal porque han sido diseñadas específicamente para corregir una holgura detectada en el modelo STL Charlotte que vas a imprimir.

Tras imprimirlas piezas tendrás que comprobar que el cilindro entre justo en la anilla.

No es necesario que pase completamente. pero si que entre ajustado haciendo algo de presión. Si lo podéis meter como en la foto siguiente, será suficiente:

![](https://gitlab.com/ma-d-kers/charlotte/-/raw/master/images/calibre_xh_1.png)

Si no hay forma de que el cilindro entre dentro de la anilla se debe modificar en el cura el parámetro Expansión horizontal:
* Si el cilindro no entraba en la anilla hay que disminuir el valor que haya (se aceptan valores negativos). Se puede ir bajando de décima de milímetro en décima de milímetro.
* Si el cilindro pasa holgado por la anilla habrá que incrementar el valor.
* Es normal que el valor resulte entre -0.2 y +0.2 (se admiten ajustes más precisos, por ejemplo -0.16)

Cuando se consiga un parámetro que permita entrar el cilindro ajustado dentro de la anilla se mantiene para utilizarlo para imprimir el modelo Charlotte valenciano.

